"""Test class for the parser."""

import re
import unittest
import dacapo.parser as parser


class TestProbesParser(unittest.TestCase):

  def test_lexer_number(self):
    var = '1'
    p = parser.Parser()
    tokens = p._lexer(var)
    self.assertEqual(len(tokens), 1)
    self.assertIn('(NUMBER,1)', tokens)

  def test_lexer_id(self):
    var = 'PERF_COUNT_HW_CPU_CYCLES'
    p = parser.Parser()
    tokens = p._lexer(var)
    self.assertEqual(len(tokens), 1)
    self.assertIn('(ID,PERF_COUNT_HW_CPU_CYCLES)', tokens)

  def test_lexer_whitespace(self):
    var = '  \n  \t'
    p = parser.Parser()
    tokens = p._lexer(var)
    self.assertEqual(len(tokens), 0)

  def test_lexer_result_header(self):
    var = 'PERF_COUNT_HW_CPU_CYCLES(all)'
    p = parser.Parser()
    tokens = p._lexer(var)
    self.assertIn('(ID,PERF_COUNT_HW_CPU_CYCLES)', tokens)
    self.assertIn('(ID,all)', tokens)

  def test_lexer_comment_header(self):
    var = '------------------------------ -----------------------------'
    p = parser.Parser()
    tokens = p._lexer(var)
    self.assertEqual(len(tokens), 0)

  def test_lexer_iterations(self):
    var = ('begin is called at iteration 1')
    p = parser.Parser()
    tokens = p._lexer(var)
    self.assertEqual(len(tokens), 6)
    self.assertIn('(KEYWORD,begin)', tokens)
    self.assertIn('(ID,is)', tokens)
    self.assertIn('(ID,called)', tokens)
    self.assertIn('(ID,at)', tokens)
    self.assertIn('(ID,iteration)', tokens)
    self.assertIn('(NUMBER,1)', tokens)

  def test_token_any_non_kw(self):
    pattern = parser._make_parser_regex([('*ANY_NON_KEYWORD*')])
    self.assertEqual(pattern, r'(\((ID|NUMBER|PUNCT)[^\(]*\))*')

  def test_token_kw(self):
    pattern = parser._make_parser_regex([('KEYWORD', 'foo')])
    self.assertEqual(pattern, r'\(KEYWORD,foo\)')

  def test_token_id(self):
    pattern = parser._make_parser_regex([('ID', '<foo>')])
    self.assertEqual(pattern, r'\(ID,(?P<foo>[^\(]+)\)')

  def test_token_number(self):
    pattern = parser._make_parser_regex([('NUMBER', '<foo>')])
    self.assertEqual(pattern, r'\(NUMBER,(?P<foo>[^\(]+)\)')

  def test_token_punct(self):
    pattern = parser._make_parser_regex([('PUNCT')])
    self.assertEqual(pattern, r'\(PUNCT\)')

  def test_token_match_gen_number(self):
    pattern = parser._make_parser_regex([('NUMBER', '<value>')])
    m = re.match(pattern, '(NUMBER,1)')
    self.assertTrue(m)
    self.assertEqual(m.group('value'), '1')

  def test_token_match_gen_id(self):
    pattern = parser._make_parser_regex([('ID', '<value>')])
    m = re.match(pattern, '(ID,foo)')
    self.assertTrue(m)
    self.assertEqual(m.group('value'), 'foo')

  def test_token_match_gen_any_non_kw(self):
    pattern = parser._make_parser_regex([('*ANY_NON_KEYWORD*')])
    m = re.match(pattern, '(ID,foo)(ID,bar)(NUMBER,3.14)(PUNCT)(NUMBER,-1)')
    self.assertTrue(m)

  def test_token_match_gen_sequence(self):
    pattern = parser._make_parser_regex([('KEYWORD,foo'), ('ID', '<id>'),
                                         ('NUMBER', '<num>')])
    m = re.match(pattern, '(KEYWORD,foo)(ID,bar)(NUMBER,0)')
    self.assertTrue(m)
    self.assertEqual(m.group('id'), 'bar')
    self.assertEqual(m.group('num'), '0')

  def test_parser_event_counter(self):
    statements = """
  begin is called at iteration 1
  ===== DaCapo 9.12 fop starting =====
  end is called at iteration 1
  report is called at iteration 2
  cycles:CPU(all):-10000000000
  ===== DaCapo 9.12 fop PASSED in 1036 msec =====
  """
    p = parser.Parser()
    self.assertRaises(ValueError, p.parse_probes, statements)

  def test_parser_negative_number(self):
    statements = """
  begin is called at iteration 1
  ===== DaCapo 9.12 fop starting =====
  end is called at iteration 1
  report is called at iteration 1
  cycles:CPU(all):-10000000000
  ===== DaCapo 9.12 fop PASSED in 1036 msec =====
  """
    p = parser.Parser()
    results = p.parse_probes(statements)
    fop_results = results['fop']
    self.assertEqual(fop_results['cycles'], ['-10000000000'])

  def test_parser_full_message(self):
    statements = """
  Setup 2 events on 4 cpus: PERF_COUNT_HW_CPU_CYCLES,PERF_COUNT_HW_INSTRUCTIONS
  Event 0:PERF_COUNT_HW_CPU_CYCLES
  Event 1:PERF_COUNT_HW_INSTRUCTIONS
  create_counter: PERF_COUNT_HW_CPU_CYCLES, on cpu -1, on task 0 as fd 3
  create_counter: PERF_COUNT_HW_INSTRUCTIONS, on cpu -1, on task 0 as fd 4
  Event_list:PERF_COUNT_HW_CPU_CYCLES,3;PERF_COUNT_HW_INSTRUCTIONS,4
  PERF_EVENT_NAMES is PERF_COUNT_HW_CPU_CYCLES,3;PERF_COUNT_HW_INSTRUCTIONS,4
  begin is called at iteration 1
  ===== DaCapo 9.12 fop starting =====
  end is called at iteration 1
  report is called at iteration 1
  ============================ Perf Counter Totals ============================
  PERF_COUNT_HW_CPU_CYCLES:CPU(all):10000000000
  PERF_COUNT_HW_INSTRUCTIONS:CPU(all):10000000001
  ------------------------------ Perf Counter Statistics -----------------------------
  cleanup is called
  ===== DaCapo 9.12 fop PASSED in 1036 msec =====
  """
    p = parser.Parser()
    results = p.parse_probes(statements)
    fop_results = results['fop']
    self.assertIn('PERF_COUNT_HW_CPU_CYCLES', fop_results)
    self.assertIn('PERF_COUNT_HW_INSTRUCTIONS', fop_results)
    self.assertEqual(fop_results['PERF_COUNT_HW_CPU_CYCLES'], ['10000000000'])
    self.assertEqual(fop_results['PERF_COUNT_HW_INSTRUCTIONS'], ['10000000001'])

  def test_parser_full_message_iteration(self):
    statements = """
  begin is called at iteration 1
  ===== DaCapo 9.12 fop starting warmup 1 =====
  end is called at iteration 1
  report is called at iteration 1
  ============================ Perf Counter warmup 1 ============================
  PERF_COUNT_HW_CPU_CYCLES:CPU(all):10000000000
  PERF_COUNT_HW_INSTRUCTIONS:CPU(all):10000000001

  ------------------------------ Perf Counter Statistics -----------------------------
  ===== DaCapo 9.12 fop completed warmup 1 in 1000 msec =====
  begin is called at iteration 2
  ===== DaCapo 9.12 fop starting =====
  end is called at iteration 2
  report is called at iteration 2
  ============================ Perf Counter ============================
  PERF_COUNT_HW_CPU_CYCLES:CPU(all):20000000000
  PERF_COUNT_HW_INSTRUCTIONS:CPU(all):20000000001

  ------------------------------ Perf Counter Statistics -----------------------------
  cleanup is called
  ===== DaCapo 9.12 fop PASSED in 200 msec =====
  """
    p = parser.Parser()
    results = p.parse_probes(statements)
    fop_results = results['fop']
    self.assertIn('PERF_COUNT_HW_CPU_CYCLES', fop_results)
    self.assertIn('PERF_COUNT_HW_INSTRUCTIONS', fop_results)
    self.assertIn('time', fop_results)
    self.assertEqual(fop_results['PERF_COUNT_HW_CPU_CYCLES'], ['10000000000',
                                                               '20000000000'])
    self.assertEqual(fop_results['PERF_COUNT_HW_INSTRUCTIONS'], ['10000000001',
                                                                 '20000000001'])
    self.assertEqual(fop_results['time'], ['1000',
                                           '200'])

  def test_parser_two_bms(self):
    statements = """
  begin is called at iteration 1
  ===== DaCapo 9.12 fop starting warmup 1 =====
  end is called at iteration 1
  report is called at iteration 1
  ============================ Perf Counter warmup 1 ============================
  cycles:CPU(all):10000000000

  ------------------------------ Perf Counter Statistics -----------------------------
  ===== DaCapo 9.12 fop completed warmup 1 in 1000 msec =====
  begin is called at iteration 2
  ===== DaCapo 9.12 fop starting =====
  end is called at iteration 2
  report is called at iteration 2
  ============================ Perf Counter ============================
  cycles:CPU(all):20000000000

  ------------------------------ Perf Counter Statistics -----------------------------
  cleanup is called
  ===== DaCapo 9.12 fop PASSED in 200 msec =====
  ------------------------------ Imaginary Transition -----------------------------
  begin is called at iteration 1
  ===== DaCapo 9.12 eclipse starting warmup 1 =====
  end is called at iteration 1
  report is called at iteration 1
  ============================ Perf Counter warmup 1 ============================
  cycles:CPU(all):30000000000

  ------------------------------ Perf Counter Statistics -----------------------------
  ===== DaCapo 9.12 eclipse completed warmup 1 in 2000 msec =====
  begin is called at iteration 2
  ===== DaCapo 9.12 eclipse starting =====
  end is called at iteration 2
  report is called at iteration 2
  ============================ Perf Counter ============================
  cycles:CPU(all):40000000000

  ------------------------------ Perf Counter Statistics -----------------------------
  cleanup is called
  ===== DaCapo 9.12 eclipse PASSED in 500 msec =====
  """
    p = parser.Parser()

    results = p.parse_probes(statements)

    fop_results = results['fop']
    self.assertIn('cycles', fop_results)
    self.assertIn('time', fop_results)
    self.assertEqual(fop_results['cycles'], ['10000000000',
                                             '20000000000'])
    self.assertEqual(fop_results['time'], ['1000',
                                           '200'])

    eclipse_results = results['eclipse']
    self.assertIn('cycles', eclipse_results)
    self.assertIn('time', eclipse_results)
    self.assertEqual(eclipse_results['cycles'], ['30000000000',
                                                 '40000000000'])
    self.assertEqual(eclipse_results['time'], ['2000',
                                               '500'])


class TestStaticAnalysisParser(unittest.TestCase):

  def test_lexer_loaded_class(self):
    var = ('[Loaded foo from bar.jar]')
    p = parser.Parser()
    tokens = p._lexer(var)
    self.assertEqual(len(tokens), 6)
    self.assertIn('(LBRACKET)', tokens)
    self.assertIn('(ID,Loaded)', tokens)
    self.assertIn('(ID,foo)', tokens)
    self.assertIn('(ID,from)', tokens)
    self.assertIn('(JAR,bar.jar)', tokens)
    self.assertIn('(RBRACKET)', tokens)

  def test_lexer_open_jar(self):
    var = ('[Opened /path/to/foo.jar]')
    p = parser.Parser()
    tokens = p._lexer(var)
    self.assertEqual(len(tokens), 4)
    self.assertIn('(LBRACKET)', tokens)
    self.assertIn('(ID,Opened)', tokens)
    self.assertIn('(JAR,/path/to/foo.jar)', tokens)
    self.assertIn('(RBRACKET)', tokens)

  def test_parser_opened_jars(self):
    statements = """
[Opened foo.jar]
[Loaded one.class from foo.jar]
[Loaded two.class from foo.jar]
[Opened bar.jar]
[Loaded uno.class from bar.jar]
[Loaded dos.class from bar.jar]
  """
    p = parser.Parser()
    results, _, _ = p.parse_static_analysis(statements)
    self.assertEqual(len(results), 0)

  def test_parser_from_file(self):
    statements = """
[Opened foo.jar]
[Loaded cool.class from foo.jar]
[Loaded lame.class from foo.jar]
[Loaded fish.class from file:bar.jar]
[Loaded robot.class from file:bar.jar]
  """
    p = parser.Parser()
    results, _, _ = p.parse_static_analysis(statements)
    self.assertEqual(len(results), 2)
    self.assertEqual(results['fish.class'], 'file:bar.jar')
    self.assertEqual(results['robot.class'], 'file:bar.jar')

  def test_compilation_print_output(self):
    statements = """
1796  478  s          java.io.BufferedOutputStream::write (67 bytes)
made not compilable on all levels  java.io.OutputStream::write
2433  674   !         java.lang.StringCoding$StringEncoder::encode (198 bytes)
===== DaCapo head-r4a4e497 fop PASSED in 1956 msec =====
  """
    p = parser.Parser()

    _, classes, methods = p.parse_static_analysis(statements)

    self.assertIn('java.io.BufferedOutputStream', classes)
    self.assertIn('java.io.OutputStream', classes)
    self.assertIn('java.lang.StringCoding$StringEncoder', classes)

    self.assertIn('java.io.BufferedOutputStream::write', methods)
    self.assertIn('java.io.OutputStream::write', methods)
    self.assertIn('java.lang.StringCoding$StringEncoder::encode', methods)

  def test_complete_sample(self):
    statements = """
[Opened rt.jar]
[Loaded org.dacapo.harness.FileDigest from file:dacapo.jar]
[Loaded org.dacapo.harness.Digest from file:dacapo.jar]
228  195             java.lang.ThreadLocal::<init> (8 bytes)
5524 5648             org.apache.fop.layoutmgr.KnuthPossPosIter::getLM (8 bytes)
[Loaded sun.security.provider.SHA from rt.jar]
5524 5649             org.apache.fop.layoutmgr.KnuthPossPosIter::next (15 bytes)
[Loaded sun.security.jca.GetInstance$1 from rt.jar]
5525 5650             org.apache.fop.layoutmgr.PositionIterator::next (32 bytes)
made not compilable on all levels org.apache.fop.layoutmgr.PositionIterator::getPos
5525  930             java.lang.Class::privateGetDeclaredConstructors (85 bytes)
6677 2999  s!         avrora.sim.clock.RippleSynchronizer::removeNode (61 bytes)
6691 3019 avrora.monitors.LEDMonitor$Mon::report (1 bytes)
===== DaCapo 9.12 fop PASSED in 4971 msec ====="""
    p = parser.Parser()
    loaded, classes, methods = p.parse_static_analysis(statements)

    self.assertEqual(len(loaded), 2)
    self.assertEqual(loaded['org.dacapo.harness.FileDigest'], 'file:dacapo.jar')
    self.assertEqual(loaded['org.dacapo.harness.Digest'], 'file:dacapo.jar')

    self.assertEqual(len(methods), 8)
    self.assertIn('java.lang.ThreadLocal::init', methods)
    self.assertIn('org.apache.fop.layoutmgr.KnuthPossPosIter::getLM', methods)
    self.assertIn('org.apache.fop.layoutmgr.PositionIterator::next', methods)
    self.assertIn('org.apache.fop.layoutmgr.PositionIterator::getPos', methods)
    self.assertIn('org.apache.fop.layoutmgr.KnuthPossPosIter::next', methods)
    self.assertIn('java.lang.Class::privateGetDeclaredConstructors', methods)
    self.assertIn('avrora.monitors.LEDMonitor$Mon::report', methods)
    self.assertIn('avrora.sim.clock.RippleSynchronizer::removeNode', methods)

    self.assertEqual(len(classes), 6)
    self.assertIn('org.apache.fop.layoutmgr.KnuthPossPosIter', classes)
    self.assertIn('org.apache.fop.layoutmgr.PositionIterator', classes)
    self.assertIn('java.lang.Class', classes)
    self.assertIn('java.lang.ThreadLocal', classes)
    self.assertIn('avrora.monitors.LEDMonitor$Mon', classes)
    self.assertIn('avrora.sim.clock.RippleSynchronizer', classes)

if __name__ == '__main__':
  unittest.main()
