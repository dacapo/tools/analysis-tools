"""Test class for the static analysis."""

import unittest
from dacapo.static_analysis import StaticAnalysis
import mock
import pandas as pd


class TestStaticAnalysis(unittest.TestCase):

  def test_init(self):
    static_analysis = StaticAnalysis()
    self.assertEqual(static_analysis.size, 0)

  def test_store(self):
    static_analysis = StaticAnalysis()
    static_analysis.store_result('class 1 2 3 4 5 6 7 8')
    self.assertEqual(static_analysis.size, 1)

  def test_bad_store(self):
    static_analysis = StaticAnalysis()
    self.assertRaises(ValueError, static_analysis.store_result,
                      'class 1 2 3 4 5 6 7')
    self.assertRaises(ValueError, static_analysis.store_result,
                      '1 2 3 4 5 6 7 8')
    self.assertRaises(ValueError, static_analysis.store_result,
                      'class 1 2 3 4 5 6 7 8 9')

  @mock.patch('pandas.DataFrame')
  def test_repr(self, mock_df):
    mock_df.return_value.__repr__ = mock.Mock(return_value='static_analysis')
    static_analysis = StaticAnalysis()
    static_analysis.store_result('class 1 2 3 4 5 6 7 8')
    str_value = repr(static_analysis)
    self.assertEqual(str_value, 'static_analysis')

  @mock.patch('pandas.Series.to_csv')
  @mock.patch('pandas.DataFrame.to_csv')
  def test_save_csv_with_static_analysis(self, mock_df_to_csv,
                                         mock_series_to_csv):
    static_analysis = StaticAnalysis()
    static_analysis.save_csv('prefix')
    calls = [mock.call('prefix_mean.csv'), mock.call('prefix_min.csv'),
             mock.call('prefix_max.csv')]

    mock_df_to_csv.assert_called_once_with('prefix.csv')
    mock_series_to_csv.assert_has_calls(calls, any_order=True)

  def test_mean(self):
    expected_df = pd.Series([2.0, 3.0, 2.0, 3.0, 4.0, 5.0, 5.0, 2.0],
                            index=['wmc', 'dit', 'noc', 'cbo',
                                   'rfc', 'lcom', 'ca', 'npm'])
    static_analysis = StaticAnalysis()
    static_analysis.store_result('class1 1 1 1 1 1 1 1 1')
    static_analysis.store_result('class2 3 5 3 5 7 9 9 3')
    self.assertTrue(static_analysis.get_mean().equals(expected_df))

  def test_min(self):
    expected_df = pd.Series([1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0],
                            index=['wmc', 'dit', 'noc', 'cbo',
                                   'rfc', 'lcom', 'ca', 'npm'])
    static_analysis = StaticAnalysis()
    static_analysis.store_result('class1 1 1 1 1 1 1 1 2')
    static_analysis.store_result('class2 2 3 4 5 6 7 8 9')
    static_analysis.store_result('class2 3 4 5 6 7 8 9 10')
    self.assertTrue(static_analysis.get_min().equals(expected_df))

  def test_max(self):
    expected_df = pd.Series([3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0],
                            index=['wmc', 'dit', 'noc', 'cbo',
                                   'rfc', 'lcom', 'ca', 'npm'])
    static_analysis = StaticAnalysis()
    static_analysis.store_result('class1 1 1 1 1 1 1 1 2')
    static_analysis.store_result('class2 2 3 4 5 6 7 8 9')
    static_analysis.store_result('class2 3 4 5 6 7 8 9 10')
    self.assertTrue(static_analysis.get_max().equals(expected_df))

if __name__ == '__main__':
  unittest.main()
