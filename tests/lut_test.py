"""Test class for the lut."""

import unittest
from dacapo.lut import LookUpTable


class TestLut(unittest.TestCase):

  def test_init(self):
    lut = LookUpTable()
    self.assertEqual(len(lut.methods), 0)
    self.assertEqual(len(lut.fields), 0)
    self.assertEqual(len(lut.classes), 0)

  def test_setup(self):
    lut = LookUpTable()
    lut.setup('tests/lut_examples')
    self.assertEqual(len(lut.methods), 5)
    self.assertEqual(len(lut.fields), 3)
    self.assertEqual(len(lut.classes), 2)

    self.assertEqual(lut.classes['122'], 'java.lang.Runnable')
    self.assertEqual(lut.classes['119'], 'java.lang.Void')
    self.assertEqual(lut.fields['37'],
                     'java.security.BasicPermissionCollection.permClass')
    self.assertEqual(lut.fields['14'],
                     'java.lang.ClassValue.version')
    self.assertEqual(lut.fields['11'],
                     'java.io.PrintWriter.lineSeparator')
    self.assertEqual(lut.methods['507'], 'java.lang.Shutdown$Lock.<init>')
    self.assertEqual(lut.methods['506'], 'java.lang.Shutdown$Lock.<init>')
    self.assertEqual(lut.methods['505'], 'java.lang.Shutdown.<clinit>')
    self.assertEqual(lut.methods['504'], 'java.lang.Shutdown.shutdown')
    self.assertEqual(lut.methods['503'], 'java.lang.Shutdown.exit')

if __name__ == '__main__':
  unittest.main()
