"""Test class for plot."""

import unittest
from dacapo.plot import Plotter
from dacapo.report import Report
import mock
import pandas as pd


class TestPlot(unittest.TestCase):

  def test_default(self):
    plotter = Plotter()
    self.assertEqual(plotter.reports, [])
    self.assertEqual(plotter.metric, None)
    self.assertEqual(plotter.normalize, None)
    self.assertEqual(plotter.per, None)
    self.assertEqual(plotter.best, True)
    self.assertEqual(plotter.separate, False)

  def test_setter(self):
    plotter = Plotter()
    plotter.reports = set()
    self.assertEqual(plotter.reports, set())
    plotter.metric = 'foo'
    self.assertEqual(plotter.metric, 'foo')
    plotter.normalize = 'bar'
    self.assertEqual(plotter.normalize, 'bar')
    plotter.per = 'baz'
    self.assertEqual(plotter.per, 'baz')
    plotter.best = False
    self.assertEqual(plotter.best, False)
    plotter.separate = True
    self.assertEqual(plotter.separate, True)

  def test_config(self):
    plotter = Plotter()
    plotter.config(reports=set(), metric='foo', normalize='bar', per='baz',
                   best=False, separate=True)
    self.assertEqual(plotter.reports, set())
    self.assertEqual(plotter.metric, 'foo')
    self.assertEqual(plotter.normalize, 'bar')
    self.assertEqual(plotter.per, 'baz')
    self.assertEqual(plotter.best, False)
    self.assertEqual(plotter.separate, True)

  def test_normalize_by_index(self):
    plotter = Plotter()
    plotter.normalize = 0
    df = pd.DataFrame({'A': [2, 4, 6, 8, 10],
                       'B': [2, 4, 6, 8, 10],
                       'C': [2, 4, 6, 8, 10]
                      })

    norm = pd.DataFrame({'A': [1.0, 2.0, 3.0, 4.0, 5.0],
                         'B': [1.0, 2.0, 3.0, 4.0, 5.0],
                         'C': [1.0, 2.0, 3.0, 4.0, 5.0]
                        })

    df = plotter.normalize_dataframe(df)
    self.assertTrue(df.equals(norm))

  def test_normalize_by_attribute(self):
    plotter = Plotter()
    plotter.normalize = 'A'
    df = pd.DataFrame({'A': [8, 12, 16, 2, 10],
                       'B': [2, 4, 6, 1, 10],
                       'C': [8, 4, 12, 4, 16]
                      })

    norm = pd.DataFrame({'A': [4.0, 6.0, 8.0, 1.0, 5.0],
                         'B': [2.0, 4.0, 6.0, 1.0, 10.0],
                         'C': [2.0, 1.0, 3.0, 1.0, 4.0]
                        })

    df = plotter.normalize_dataframe(df)
    self.assertTrue(df.equals(norm))

  def test_scale_by_metric(self):
    plotter = Plotter()
    plotter.metric = 'B'
    plotter.per = 'A'
    df = pd.DataFrame({'A': [1, 2, 3, 4, 5],
                       'B': [2, 4, 6, 8, 10]
                      })

    norm = pd.DataFrame({'A': [1, 2, 3, 4, 5],
                         'B': [2, 4, 6, 8, 10],
                         'B per A': [2.0, 2.0, 2.0, 2.0, 2.0]
                        })

    df = plotter.scale_column(df)

    self.assertTrue(df.equals(norm))

  def test_normalize_bad_value(self):
    plotter = Plotter()
    plotter.normalize = 1.5
    df = pd.DataFrame({'A': [2, 4, 6, 8, 10],
                       'B': [2, 4, 6, 8, 10],
                       'C': [2, 4, 6, 8, 10]
                      })
    self.assertRaises(TypeError, plotter.normalize_dataframe, df)

  def test_normalize_neg_index(self):
    plotter = Plotter()
    df = pd.DataFrame({'A': [2, 4, 6, 8, 10],
                       'B': [2, 4, 6, 8, 10],
                       'C': [2, 4, 6, 8, 10]
                      })

    plotter.normalize = -1
    self.assertRaises(IndexError, plotter.normalize_dataframe, df)

    plotter.normalize = 6
    self.assertRaises(IndexError, plotter.normalize_dataframe, df)

  def test_normalize_bad_metric(self):
    plotter = Plotter()
    df = pd.DataFrame({'A': [2, 4, 6, 8, 10],
                       'B': [2, 4, 6, 8, 10],
                       'C': [2, 4, 6, 8, 10]
                      })
    plotter.normalize = 'D'
    self.assertRaises(ValueError, plotter.normalize_dataframe, df)

  @mock.patch.object(Plotter, '_output_plot')
  @mock.patch.object(pd.DataFrame, 'plot')
  def test_log_plot(self, mock_plot, mock_output_plot):
    report = Report()
    report.add_dyn_analysis('test', 0, {'A': [5.0, 10.0, 23.0]})
    plotter = Plotter()
    plotter.config(reports=[report], bm='test', metric='A', log=True)
    plotter.plot()

    _, kwargs = mock_plot.call_args

    self.assertIn('logy', kwargs)
    self.assertTrue(kwargs['logy'])

    mock_output_plot.assert_called_once()

  @mock.patch.object(Plotter, '_output_plot')
  @mock.patch.object(pd.DataFrame, 'plot')
  def test_log_plot_separate(self, mock_plot, mock_output_plot):
    report = Report()
    report.add_dyn_analysis('test', 0, {'A': [5.0, 10.0, 23.0]})
    plotter = Plotter()
    plotter.config(reports=[report], bm='test', separate=True, metric='A',
                   log=True)
    plotter.plot()

    arg_list = mock_plot.call_args_list
    self.assertGreater(len(arg_list), 0)

    for args in arg_list:
      _, kwargs = args
      self.assertIn('logy', kwargs)
      self.assertTrue(kwargs['logy'])
      mock_output_plot.assert_called_once()

  def test_mean_dataframe(self):

    data = {'A': [5.0, 10.0, 23.0]}
    err = {'A': [0.0, 0.0, 0.0]}

    expected_df = pd.DataFrame(data)
    expected_df_err = pd.DataFrame(err)

    report = Report()
    report.add_dyn_analysis('test', 0, data)
    report.add_dyn_analysis('test', 1, data)

    plotter = Plotter()
    plotter.config(reports=[report], bm='test', metric='A')
    df, df_err, lim = plotter.get_mean_dataframe()

    self.assertTrue(df.iloc[:, 0].equals(expected_df.iloc[:, 0]))
    self.assertTrue(df_err.iloc[:, 0].equals(expected_df_err.iloc[:, 0]))
    self.assertEqual(lim, [5.0, 23.0])

  def test_normalize_dataframe(self):

    expected_df_1 = pd.Series([1.0, 2.0, 3.0])
    expected_df_err_1 = pd.Series([0.0, 0.0, 0.0])
    report_1 = Report()
    report_1.add_dyn_analysis('test', 0, {'A': [5.0, 10.0, 15.0]})
    report_1.add_dyn_analysis('test', 1, {'A': [5.0, 10.0, 15.0]})

    expected_df_2 = pd.Series([1.0, 2.5])
    expected_df_err_2 = pd.Series([0.0, 0.0])
    report_2 = Report()
    report_2.add_dyn_analysis('test', 0, {'A': [2.0, 5.0]})
    report_2.add_dyn_analysis('test', 1, {'A': [4.0, 10.0]})

    plotter = Plotter()
    plotter.config(reports=[report_1, report_2], bm='test', metric='A')

    df, df_err, max_iter = plotter.get_norm_dataframes()

    self.assertTrue(df.iloc[:, 0].equals(expected_df_1))
    self.assertTrue(df.iloc[0:2, 1].equals(expected_df_2))

    self.assertTrue(df_err.iloc[:, 0].equals(expected_df_err_1))
    self.assertTrue(df_err.iloc[0:2, 1].equals(expected_df_err_2))

    self.assertEqual(max_iter, 3)

  @mock.patch.object(Plotter, '_output_plot')
  def test_plot(self, mock_output_plot):
    report = Report()
    report.add_dyn_analysis('test', 0, {'A': [5.0, 10.0, 23.0]})
    report.add_dyn_analysis('test', 1, {'A': [3.0, 12.0, 17.0]})
    plotter = Plotter()
    plotter.config(reports=[report], bm='test', metric='A')
    df_expected, df_err_expected, _ = plotter.get_mean_dataframe()
    _, _, df, df_err = plotter.plot()

    self.assertTrue(df.equals(df_expected))
    self.assertTrue(df_err.equals(df_err_expected))
    mock_output_plot.assert_called_once()

if __name__ == '__main__':
  unittest.main()
