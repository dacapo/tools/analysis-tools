"""Test class for the dynamic analysis."""

import unittest
from dacapo.dynamic_analysis import DynamicAnalysis
import mock
import pandas as pd


class TestDynamicAnalysis(unittest.TestCase):

  _ref_df = pd.DataFrame({'A': [0, 1, 2],
                          'B': [0, 1, 2]})

  def test_init(self):
    dyn_analysis = DynamicAnalysis()
    self.assertEqual(dyn_analysis.size, 0)

  def test_add(self):
    dyn_analysis = DynamicAnalysis()
    dyn_analysis.add_invocation(0, self._ref_df)
    self.assertEqual(dyn_analysis.size, 1)

  def test_get(self):
    """Should get the same thing back twice."""
    dyn_analysis = DynamicAnalysis()
    dyn_analysis.add_invocation(0, self._ref_df)

    self.assertTrue(dyn_analysis.get_invocation(0)
                    .equals(dyn_analysis.get_invocation(0)))

    self.assertTrue(dyn_analysis.get_invocation(0).equals(self._ref_df))

  def test_repr_empty(self):
    dyn_analysis = DynamicAnalysis()
    str_value = repr(dyn_analysis)
    self.assertEqual(str_value, '')

  def test_repr(self):
    dyn_analysis = DynamicAnalysis()
    dyn_analysis.add_invocation(0, self._ref_df)
    str_value = repr(dyn_analysis)
    self.assertEqual(str_value, '\n'.join(['Invocation %s\n%s' %
                                           (1, str(self._ref_df))]))

  @mock.patch('pandas.DataFrame.to_csv')
  def test_save_csv(self, mock_to_csv):
    dyn_analysis = DynamicAnalysis()
    dyn_analysis.add_invocation(0, self._ref_df)
    dyn_analysis.save_csv('prefix')
    mock_to_csv.assert_called_once_with('prefix_invocation_0.csv')

  @mock.patch('pandas.DataFrame.to_csv')
  def test_save_csv_multiple_invocations(self, mock_to_csv):
    dyn_analysis = DynamicAnalysis()
    dyn_analysis.add_invocation(0, self._ref_df)
    dyn_analysis.add_invocation(1, self._ref_df)

    calls = [mock.call('prefix_invocation_0.csv'),
             mock.call('prefix_invocation_1.csv')]

    dyn_analysis.save_csv('prefix')
    mock_to_csv.assert_has_calls(calls)

  def test_adding_to_existing_invocation(self):
    expected_df = pd.DataFrame({'A': [0, 1, 2],
                                'B': [0, 1, 2],
                                'C': [3, 4, 5],
                                'D': [3, 4, 5]})

    new_df = pd.DataFrame({'C': [3, 4, 5],
                           'D': [3, 4, 5]})

    dyn_analysis = DynamicAnalysis()
    dyn_analysis.add_invocation(0, self._ref_df)
    dyn_analysis.add_invocation(0, new_df)
    dyn_analysis.add_invocation(1, new_df)

    actual_df_0 = dyn_analysis.get_invocation(0)
    actual_df_1 = dyn_analysis.get_invocation(1)

    self.assertTrue(actual_df_0.equals(expected_df[actual_df_0.columns]))
    self.assertTrue(actual_df_1.equals(new_df[actual_df_1.columns]))

  def test_bad_invocation(self):
    dyn_analysis = DynamicAnalysis()
    self.assertRaises(IndexError, dyn_analysis.add_invocation, 1, self._ref_df)

  def test_adding_duplicated_entries(self):
    expected_df = pd.DataFrame({'A': [1, 2, 3],
                                'B': [1, 2, 3],
                                'C': [2, 2, 4]})
    df_1 = pd.DataFrame({'A': [1, 2, 3], 'B': [3, 4, 5]})
    df_2 = pd.DataFrame({'B': [1, 2, 3], 'C': [2, 2, 4]})

    dyn_analysis = DynamicAnalysis()
    dyn_analysis.add_invocation(0, df_1)
    dyn_analysis.add_invocation(0, df_2)
    actual_df = dyn_analysis.get_invocation(0)
    self.assertTrue(actual_df.equals(expected_df[actual_df.columns]))

if __name__ == '__main__':
  unittest.main()
