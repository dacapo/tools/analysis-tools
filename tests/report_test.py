"""Test class for the report."""

import unittest
from dacapo.report import Report
import mock
import pandas as pd


class TestReport(unittest.TestCase):

  def test_size_dyn_analysis(self):
    report = Report()
    self.assertEqual(report.number_of_dyn_analyses, 0)

  def test_size_static_analysis(self):
    report = Report()
    self.assertEqual(report.number_of_static_analyses, 0)

  def test_add_dyn_analysis(self):
    report = Report()
    report.add_dyn_analysis('fop', 0, {})
    self.assertEqual(report.number_of_dyn_analyses, 1)

  def test_init_state_dyn_analysis(self):
    report = Report()
    report.add_dyn_analysis('fop', 0, {})

    self.assertTrue(report.get_dyn_analysis('fop'), {})

  def test_add_distict_bm(self):
    report = Report()
    report.add_dyn_analysis('fop', 0, {})
    report.add_dyn_analysis('batik', 0, {})
    self.assertEqual(report.number_of_dyn_analyses, 2)

  def test_unknown_bm(self):
    report = Report()
    self.assertRaises(ValueError, report.get_dyn_analysis, 'unknown')

  def test_retrieve_dyn_analysis_value(self):
    report = Report()
    test_result = {'cycles': [0, 1]}
    ref = pd.DataFrame(test_result)
    report.add_dyn_analysis('fop', 0, test_result)
    self.assertEqual(report.number_of_dyn_analyses, 1)
    self.assertTrue(report.get_dyn_analysis('fop').get_invocation(0)
                    .equals(ref))

  def test_repr_empty_report(self):
    report = Report()
    str_value = ''
    self.assertEqual(repr(report), str_value)

  @mock.patch('dacapo.dynamic_analysis.DynamicAnalysis.__repr__')
  def test_report_with_dyn_analysis_repr(self, mock_repr):
    report = Report()
    mock_repr.return_value = 'results'
    report.add_dyn_analysis('fop', 0, {})
    expected_value = '### Dynamic Analysis ###\nBenchmark: fop\nresults'
    self.assertEqual(repr(report), expected_value)

  @mock.patch('dacapo.static_analysis.StaticAnalysis.__repr__')
  def test_report_with_static_analysis_repr(self, mock_repr):
    report = Report()
    mock_repr.return_value = 'results'
    report.add_static_analysis('fop', ['class 1 2 3 4 5 6 7 8'])
    expected_value = '### Static Analysis ###\nBenchmark: fop\nresults'
    self.assertEqual(repr(report), expected_value)

  @mock.patch('dacapo.static_analysis.StaticAnalysis.__repr__')
  @mock.patch('dacapo.dynamic_analysis.DynamicAnalysis.__repr__')
  def test_report_repr(self, mock_dyn_repr, mock_st_repr):
    report = Report()
    mock_dyn_repr.return_value = 'results'
    mock_st_repr.return_value = 'results'

    report.add_dyn_analysis('fop', 0, {})
    report.add_static_analysis('fop', ['class 1 2 3 4 5 6 7 8'])

    expected_value = '\n'.join(['### Dynamic Analysis ###', 'Benchmark: fop',
                                'results', '### Static Analysis ###',
                                'Benchmark: fop', 'results'])

    self.assertEqual(repr(report), expected_value)

  @mock.patch('dacapo.static_analysis.StaticAnalysis.save_csv')
  @mock.patch('dacapo.dynamic_analysis.DynamicAnalysis.save_csv')
  def test_save_csv(self, mock_dyn_to_csv, mock_static_to_csv):
    report = Report()
    report.save_csv()
    mock_dyn_to_csv.assert_not_called()
    mock_static_to_csv.assert_not_called()

  @mock.patch('dacapo.dynamic_analysis.DynamicAnalysis.save_csv')
  def test_save_csv_with_dyn_analysis(self, mock_to_csv):
    report = Report()
    report.add_dyn_analysis('fop', 0, {})
    report.save_csv()
    mock_to_csv.assert_called_once()

  @mock.patch('dacapo.static_analysis.StaticAnalysis.save_csv')
  def test_save_csv_with_static_analysis(self, mock_to_csv):
    report = Report()
    report.add_static_analysis('fop', ['class 1 2 3 4 5 6 7 8'])
    report.save_csv()
    mock_to_csv.assert_called_once()

if __name__ == '__main__':
  unittest.main()
