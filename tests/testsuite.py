"""Run all test cases."""

import unittest
from tests.command_builder_test import TestCommandBuilder
from tests.dynamic_analysis_test import TestDynamicAnalysis
from tests.lut_test import TestLut
from tests.parser_test import TestProbesParser
from tests.parser_test import TestStaticAnalysisParser
from tests.plot_test import TestPlot
from tests.report_test import TestReport
from tests.run_test import TestRun
from tests.static_analysis_test import TestStaticAnalysis
from tests.util_test import TestUtil


def _get_test_cases():
  tests = [
      unittest.TestLoader().loadTestsFromTestCase(TestRun),
      unittest.TestLoader().loadTestsFromTestCase(TestProbesParser),
      unittest.TestLoader().loadTestsFromTestCase(TestStaticAnalysisParser),
      unittest.TestLoader().loadTestsFromTestCase(TestStaticAnalysis),
      unittest.TestLoader().loadTestsFromTestCase(TestUtil),
      unittest.TestLoader().loadTestsFromTestCase(TestDynamicAnalysis),
      unittest.TestLoader().loadTestsFromTestCase(TestReport),
      unittest.TestLoader().loadTestsFromTestCase(TestPlot),
      unittest.TestLoader().loadTestsFromTestCase(TestCommandBuilder),
      unittest.TestLoader().loadTestsFromTestCase(TestLut),
  ]

  return tests


if __name__ == '__main__':
  testsuite = unittest.TestSuite(_get_test_cases())
  unittest.TextTestRunner().run(testsuite)
