"""Test class for command_builder."""

import unittest
import dacapo.command_builder


class TestCommandBuilder(unittest.TestCase):

  def test_default(self):
    builder = dacapo.command_builder.CommandBuilder()
    self.assertEqual(builder.w, 1)
    self.assertEqual(builder.jdk, 'java')
    self.assertEqual(builder.dacapo, 'dacapo-9.12-bach.jar')
    self.assertEqual(builder.events, 'cycles')
    self.assertEqual(builder.compiled, False)
    self.assertEqual(builder.interpreted, False)
    self.assertEqual(builder.static_analysis, False)
    self.assertEqual(builder.probes_path, 'probes')
    self.assertEqual(builder.ckjm, 'ckjm.jar')

  def test_construct_jdk_cmd(self):

    builder = dacapo.command_builder.CommandBuilder()
    cmd = builder.construct_jdk_cmd()
    self.assertEqual(cmd, ['java', '-Djava.library.path=probes',
                           '-Dprobes=PerfEventLauncher', '-cp',
                           'probes/probes.jar:dacapo-9.12-bach.jar'
                          ])

    builder.probes_path = 'foo/bar'
    cmd = builder.construct_jdk_cmd()
    self.assertEqual(cmd, ['java', '-Djava.library.path=foo/bar',
                           '-Dprobes=PerfEventLauncher', '-cp',
                           'foo/bar/probes.jar:dacapo-9.12-bach.jar'
                          ])

    builder.interpreted = True
    builder.compiled = False
    cmd = builder.construct_jdk_cmd()

    self.assertEqual(cmd, ['java', '-Djava.compiler=NONE',
                           '-Djava.library.path=foo/bar',
                           '-Dprobes=PerfEventLauncher', '-cp',
                           'foo/bar/probes.jar:dacapo-9.12-bach.jar'
                          ])

    builder.interpreted = False
    builder.compiled = True
    cmd = builder.construct_jdk_cmd()

    self.assertEqual(cmd, ['java', '-XX:CompileThreshold=1',
                           '-Djava.library.path=foo/bar',
                           '-Dprobes=PerfEventLauncher', '-cp',
                           'foo/bar/probes.jar:dacapo-9.12-bach.jar'
                          ])

    builder.interpreted = False
    builder.compiled = False
    builder.static_analysis = True
    cmd = builder.construct_jdk_cmd()

    self.assertEqual(cmd, ['java', '-XX:CompileThreshold=1', '-verbose:class',
                           '-XX:+PrintCompilation', '-cp',
                           'dacapo-9.12-bach.jar'
                          ])

  def test_construct_harness(self):

    builder = dacapo.command_builder.CommandBuilder()
    cmd = builder.construct_harness_command('fop')

    self.assertEqual(cmd, ['Harness', '-c', 'probe.DacapoBachCallback', '-n',
                           '1', 'fop'])

    builder.w = 9
    cmd = builder.construct_harness_command('fop')
    self.assertEqual(cmd, ['Harness', '-c', 'probe.DacapoBachCallback', '-n',
                           '9', 'fop'])

    builder.callback = 'probe.Dacapo2006Callback'
    cmd = builder.construct_harness_command('fop')
    self.assertEqual(cmd, ['Harness', '-c', 'probe.Dacapo2006Callback', '-n',
                           '9', 'fop'])

  def test_construct_static_analysis(self):

    builder = dacapo.command_builder.CommandBuilder()
    builder.static_analysis = True
    cmd = builder.construct_static_analysis_cmd('fop')

    self.assertEqual(cmd, ['java', '-XX:CompileThreshold=1', '-verbose:class',
                           '-XX:+PrintCompilation', '-cp',
                           'dacapo-9.12-bach.jar', 'Harness', '-n', '1',
                           'fop'])

  def test_construct_perf(self):

    builder = dacapo.command_builder.CommandBuilder()
    cmd = builder.construct_perf_cmd('fop')

    self.assertEqual(cmd, [
        'probes/perf_event_launcher', 'cycles', 'java',
        '-Djava.library.path=probes', '-Dprobes=PerfEventLauncher', '-cp',
        'probes/probes.jar:dacapo-9.12-bach.jar', 'Harness', '-c',
        'probe.DacapoBachCallback', '-n', '1', 'fop'
    ])

    builder.probes_path = 'foo/bar'
    cmd = builder.construct_perf_cmd('fop')

    self.assertEqual(cmd, [
        'foo/bar/perf_event_launcher', 'cycles', 'java',
        '-Djava.library.path=foo/bar', '-Dprobes=PerfEventLauncher', '-cp',
        'foo/bar/probes.jar:dacapo-9.12-bach.jar', 'Harness', '-c',
        'probe.DacapoBachCallback', '-n', '1', 'fop'
    ])

  def test_ckjm(self):

    builder = dacapo.command_builder.CommandBuilder()
    cmd = builder.construct_ckjm_cmd()

    self.assertEqual(cmd, ['java', '-jar', 'ckjm.jar'])

    builder.ckjm = 'ckjm/ckjm.jar'
    cmd = builder.construct_ckjm_cmd()
    self.assertEqual(cmd, ['java', '-jar', 'ckjm/ckjm.jar'])

if __name__ == '__main__':
  unittest.main()
