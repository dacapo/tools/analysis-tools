"""Test class for util."""

import subprocess
import unittest
import dacapo.util as util
import mock


class TestUtil(unittest.TestCase):

  def test_execute_fail_cmd(self):
    cmd = ['false']
    self.assertRaises(RuntimeError, util.execute_cmd, cmd)

  def test_cmd_output(self):
    cmd = ['echo', 'Hello World']
    out = util.execute_cmd(cmd)
    self.assertEqual(out, 'Hello World\n')

  @mock.patch('subprocess.Popen')
  def test_exec_cmd_default(self, mock_popen):
    mock_popen.return_value.communicate.return_value = ('out', '')
    mock_popen.return_value.returncode = 0
    cmd = ['Mock']
    out = util.execute_cmd(cmd)

    self.assertEqual(out, 'out')
    mock_popen.assert_called_once_with(cmd, stderr=subprocess.STDOUT,
                                       stdout=subprocess.PIPE, stdin=None)

  @mock.patch('subprocess.Popen')
  def test_exec_cmd_no_stderr(self, mock_popen):
    mock_popen.return_value.communicate.return_value = ('out', '')
    mock_popen.return_value.returncode = 0
    cmd = ['Mock']
    out = util.execute_cmd(cmd, include_stderr=False)

    self.assertEqual(out, 'out')
    mock_popen.assert_called_once_with(cmd, stderr=subprocess.PIPE,
                                       stdout=subprocess.PIPE, stdin=None)

  @mock.patch('subprocess.Popen')
  def test_exec_cmd_with_input(self, mock_popen):
    mock_popen.return_value.communicate.return_value = ('out', '')
    mock_popen.return_value.returncode = 0
    cmd = ['Mock']
    out = util.execute_cmd(cmd, include_stderr=True, input_value='Input')

    self.assertEqual(out, 'out')
    mock_popen.assert_called_once_with(cmd, stderr=subprocess.STDOUT,
                                       stdout=subprocess.PIPE,
                                       stdin=subprocess.PIPE)

    args, _ = mock_popen.return_value.communicate.call_args

    self.assertEqual(len(args), 1)
    self.assertIn('Input', args)

  @mock.patch('subprocess.Popen')
  def test_exec_cmd_bad_return_code(self, mock_popen):
    mock_popen.return_value.communicate.return_value = ('out', '')
    mock_popen.return_value.returncode = 1
    cmd = ['Mock']
    self.assertRaises(RuntimeError, util.execute_cmd, cmd)

if __name__ == '__main__':
  unittest.main()
