"""Test class for run."""

import unittest
from dacapo.run import Run
import mock


class TestRun(unittest.TestCase):

  def test_default(self):
    my_run = Run()
    self.assertEqual(my_run.n, 1)

  def test_config(self):
    my_run = Run()
    my_run.config(w=3, n=2, interpreted=True, dacapo='dacapo.jar',
                  bms='eclipse', compiled=True, static_analysis=True,
                  events='branches', label='my run')
    self.assertEqual(my_run.command_builder.w, 3)
    self.assertEqual(my_run.command_builder.interpreted, True)
    self.assertEqual(my_run.command_builder.dacapo, 'dacapo.jar')
    self.assertEqual(my_run.command_builder.bms, 'eclipse')
    self.assertEqual(my_run.command_builder.compiled, True)
    self.assertEqual(my_run.command_builder.static_analysis, True)
    self.assertEqual(my_run.command_builder.events, 'branches')
    self.assertEqual(my_run.n, 2)
    self.assertEqual(my_run.report.label, 'my run')

  def test_get_benchmarks(self):
    bms = ['avrora', 'batik', 'eclipse', 'fop', 'h2', 'jython', 'luindex',
           'lusearch', 'pmd', 'sunflow', 'tomcat', 'tradebeans', 'tradesoap',
           'xalan'
          ]
    my_run = Run()
    my_run.command_builder.bms = None
    for bm in my_run.get_benchmarks():
      self.assertIn(bm, bms)

  @mock.patch('dacapo.util.execute_cmd')
  def test_run_perf(self, mock_execute_cmd):
    dacapo_printout = '''begin is called at iteration 1
  ===== DaCapo 9.12 fop starting =====
  end is called at iteration 1
  report is called at iteration 1
  ============================ Perf Counter warmup 1 ============================
  cycles:CPU(all):10000000000
  instructions:CPU(all):10000000001
  ------------------------------ Perf Counter Statistics -----------------------------
  cleanup is called
  ===== DaCapo 9.12 fop PASSED in 200 msec ====='''

    mock_execute_cmd.return_value = dacapo_printout

    my_run = Run()
    my_run.config(bms='fop', events='cycles,instructions')

    report = my_run.run_perf()
    self.assertEqual(report.number_of_dyn_analyses, 1)
    self.assertEqual(report.bms, ['fop'])

    dyn_analysis = report.get_dyn_analysis('fop')
    self.assertEqual(dyn_analysis.size, 1)

    invocation = dyn_analysis.get_invocation(0)
    self.assertEqual(invocation['cycles'][0], 10000000000)
    self.assertEqual(invocation['instructions'][0], 10000000001)
    self.assertEqual(invocation['time'][0], 200)

    mock_execute_cmd.assert_called_once()

  @mock.patch('dacapo.util.execute_cmd')
  def test_run_ckjm(self, mock_execute_cmd):
    execution_output = 'ignore'
    ckjm_output = '''Bar 1 2 3 4 5 6 7 8'''

    mock_execute_cmd.side_effect = [execution_output, ckjm_output]

    my_run = Run()
    my_run.config(static_analysis=True, bms='fop')

    report = my_run.run_ckjm()
    self.assertEqual(report.number_of_static_analyses, 1)

    result = report.get_static_analysis('fop').get_result_of_class('Bar')

    self.assertEqual(result['wmc'], 1.0)
    self.assertEqual(result['dit'], 2.0)
    self.assertEqual(result['noc'], 3.0)
    self.assertEqual(result['cbo'], 4.0)
    self.assertEqual(result['rfc'], 5.0)
    self.assertEqual(result['lcom'], 6.0)
    self.assertEqual(result['ca'], 7.0)
    self.assertEqual(result['npm'], 8.0)

  @mock.patch('dacapo.util.execute_cmd')
  @mock.patch.object(Run, '_run_loaded')
  def test_filtered_classes_from_ckjm(self, mock_run_loaded, mock_execute_cmd):
    loaded_classes = '\n'.join(['[Loaded Bar from file:foo.jar]',
                                '0  1   ! Bar::run (198 bytes)',
                                '[Loaded Baz from file:foo.jar]'
                               ])

    ckjm_output = '''Bar 1 2 3 4 5 6 7 8'''

    mock_execute_cmd.side_effect = [loaded_classes, ckjm_output]

    my_run = Run()
    my_run.config(static_analysis=True, bms='fop')

    my_run.run_ckjm()

    mock_run_loaded.assert_called_with('fop', {'Bar': 'file:foo.jar'})


if __name__ == '__main__':
  unittest.main()
