# DaCapo Analysis Tooling

## Overview
This project consists of analysis tooling for selecting Java benchmarks.

## Dependencies

This project depends on a few tools for analysis. Follow the installation instructions
from each tool before using the scripts provided in this project.

- [probes](https://gitlab.anu.edu.au/dacapo/tools/probes)
- [ckjm](https://github.com/dspinellis/ckjm)
- [elephant tracks 2](https://github.com/ElephantTracksProject/et2-java)

In order to use the scripts, you need to install the package dependencies:

```pip install -r requirements.txt```

## Usage

In order to use the scripts, execute the following command:

```python -m dacapo.analyzer [--help] [--w=val] [--n=val] [--jdk=val] [--dacapo=val] [--events=val]
   [--static_analysis] [--bms=val]  [--all] [--probes_path=val] [--ckjm=val]```

This will perform static or dynamic analysis depending on the values provided.

* If events are specified using the --events option, then dynamic analysis will 
be performed. 

* If a jar file is specified using the --dacapo option and no event is 
specified, then static analysis will be performed on the given jar file. 

* Finally, if the --static_analysis flag is specified, then static analysis is
performed on loaded classes during execution of a given benchmark.

optional arguments:

| Flag                | Description
| --------------------| ---------------------------|
|  --help             | show help message and exit|
|  --w=val             | number of warmup iterations.|
|  --n=val             | number of invocations. |
|  --jdk val          | the jdk to use. |
|  --dacapo=val       | the DaCapo jar file to use.|
|  --events=val       | the perf events to measure.|
|  --static_analysis  | used to perform static analysis. |
|  --bms=val          | the benchmarks to run.<br/><br/><b>Options:</b> avrora, batik, eclipse, fop, h2,<br/> jython, luindex, lusearch, pmd, sunflow,<br/> tomcat, tradebeans, tradesoap, and xalan. |
|  --all              | runs all benchmarks. |
|  --probes_path=val  | the path to the probes tool. |
|  --ckjm=val          | the ckjm jar file to use. |


## Testing

```python -m tests.testsuite```


## License
 This work is released under the [Apache 2.0 license](LICENSE).
