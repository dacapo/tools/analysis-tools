"""Stores data from dynamic analysis."""
from absl import logging
import pandas as pd


class DynamicAnalysis(object):
  """Results for dynamic analysis.

  DynamicAnalysis objects store data for a collection of benchmarks.

  Instance Attributes:
    _benchmarks: a list of dataframes, one for each iteration, where each one
    stores analysis results for its corresponding iteration.
  """

  def __init__(self):
    """Constructs a new DynamicAnalysis."""
    self._benchmarks = []

  def add_invocation(self, invocation, df):
    """Stores the results for a new invocation.

    Args:
      invocation: the invocation index that the result corresponds to.
      df: a dataframe containing the dynamic analysis results for a new
      invocation.
    Raises:
      IndexError: when the user tries to add a dynamic results at invocation 'n'
      but there is no result for invocation 'n-1'.
    """
    logging.debug('Adding new dynamic analysis invocation')
    num_df = df.apply(pd.to_numeric)

    if invocation < len(self._benchmarks):
      concat_df = pd.concat([num_df, self._benchmarks[invocation]], axis=1)
      duplicated_columns = concat_df.columns.duplicated()
      self._benchmarks[invocation] = concat_df.loc[:, ~duplicated_columns]
    elif invocation == len(self._benchmarks):
      self._benchmarks.append(num_df)
    else:
      raise IndexError(('Tried adding dynamic analysis result for invocation '
                        '%s but there is no result for entry'
                        '%s.') % (invocation, invocation-1))

  def get_invocation(self, index):
    """Gets the results for an invocation.

    Args:
      index: an integer to select a particular benchmark run.

    Returns:
      a dataframe with the results from the specified run.

    Raises:
      IndexError: if there is an attempt to retrieve the results of a particular
      benchmark run but the given index is out of bounds.
    """
    logging.debug('Getting invocation %s', index)
    if index < 0 or index >= len(self._benchmarks):
      if len(self._benchmarks) is 0:
        msg = ('Tried to get dynamic analysis at index %s '
               'but no invocation has been created.') % index
      else:
        msg = ('Tried to get dynamic analysis at index %s '
               'but the given index value is out of range valid. The value '
               'should be within [0, %s].' % (index, len(self._benchmarks)-1))

      raise IndexError(msg)

    return self._benchmarks[index]

  def save_csv(self, prefix):
    """Saves all invocations to a csv file."""
    for i in range(len(self._benchmarks)):
      df = self._benchmarks[i]
      df.to_csv('%s_invocation_%s.csv' % (prefix, i))

  @property
  def size(self):
    """Returns the number of dynamic analysis results."""
    return len(self._benchmarks)

  def __repr__(self):
    invocations = range(1, len(self._benchmarks)+1)
    return '\n'.join(['Invocation %s\n%s' % pair
                      for pair in zip(invocations, self._benchmarks)])

