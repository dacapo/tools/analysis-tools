"""Provides functions to build commands to run different analysis tools."""
import os


class CommandBuilder(object):
  """Class instance for building commands for running analysis tools.

  Instance Attributes:
    w: an integer that represents the number of iterations.

    jdk: a string to indicate the location of the jdk.

    dacapo: a string to indicate the location of the dacapo jar file.

    bms: a string to specify the benchmarks to run. This should have benchmarks
         separated by commas (e.g. fop,avrora,jython).

    compiled: a boolean to construct commands in compiled mode.

    interpreted: a boolean to construct commands in interpreter mode.

    static_analysis: a boolean to indicate that static analysis should be
                     executed instead of dynamic analysis.

    probes_path: a string to indicate the path to the probes tool.

    ckjm: a string to indicate the location of the ckjm tool.

    callback: dacapo callback that will be used when invoking the harness.
  """

  _INTERPRETER_FLAG = '-Djava.compiler=NONE'
  _COMPILED_FLAG = '-XX:CompileThreshold=1'
  _PRINT_COMPILATION_FLAG = '-XX:+PrintCompilation'
  _LOADED_CLASSES_FLAG = '-verbose:class'

  def __init__(self, w=1, jdk='java', dacapo='dacapo-9.12-bach.jar',
               events='cycles', bms='fop', compiled=False, interpreted=False,
               static_analysis=False, probes_path='probes', ckjm='ckjm.jar',
               callback='probe.DacapoBachCallback'
              ):

    self.w = w
    self.jdk = jdk
    self.dacapo = dacapo
    self.events = events
    self.bms = bms
    self.compiled = compiled
    self.interpreted = interpreted
    self.static_analysis = static_analysis
    self.probes_path = probes_path
    self.ckjm = ckjm
    self.callback = callback

  def construct_jdk_cmd(self):
    """Constructs the command for running a Java application."""

    probes = os.path.join(self.probes_path, 'probes.jar')

    cmd = [self.jdk]

    if self.static_analysis:
      cmd.append(self._COMPILED_FLAG)
      cmd.append(self._LOADED_CLASSES_FLAG)
      cmd.append(self._PRINT_COMPILATION_FLAG)
      cmd.extend(['-cp', self.dacapo])
    else:
      ## Here we can allow extra args
      if self.interpreted:
        cmd.append(self._INTERPRETER_FLAG)
      elif self.compiled:
        cmd.append(self._COMPILED_FLAG)

      cmd.extend(['-Djava.library.path=%s' % self.probes_path,
                  '-Dprobes=PerfEventLauncher',
                  '-cp',
                  '%s:%s' % (probes, self.dacapo)
                 ])
    return cmd

  def construct_harness_command(self, bm):
    """Constructs command that invokes the DaCapo harness."""
    cmd = ['Harness']

    if not self.static_analysis:
      cmd.extend(['-c', self.callback])
    cmd.extend(['-n', str(self.w), bm])

    return cmd

  def construct_perf_cmd(self, bm):
    """Constructs the command to setup perf probe and invoke the Harness."""

    # Probes command
    # TODO(lwatanabe01): this assumes the probes are located in the current
    # directory.
    probes_launcher = os.path.join(self.probes_path, 'perf_event_launcher')
    cmd = [probes_launcher, self.events]
    cmd.extend(self.construct_jdk_cmd())
    cmd.extend(self.construct_harness_command(bm))
    return cmd

  def construct_static_analysis_cmd(self, bm):
    """Constructs the command to run static analysis."""
    cmd = self.construct_jdk_cmd()
    cmd.extend(self.construct_harness_command(bm))
    return cmd

  def construct_ckjm_cmd(self):
    """Constructs the command to run ckjm."""
    cmd = [self.jdk, '-jar', self.ckjm]
    return cmd
