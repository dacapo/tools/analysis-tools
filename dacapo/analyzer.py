"""Main function to run the scripts."""

from absl import app
from absl import flags
from absl import logging

from dacapo.run import Run

FLAGS = flags.FLAGS

flags.DEFINE_boolean('debug', False, 'run scripts in debug mode.')
flags.DEFINE_string('dacapo', None, 'the DaCapo jar to use.')
flags.DEFINE_integer('w', None, 'number of warmup iterations.', lower_bound=0)
flags.DEFINE_integer('n', None, 'number of invocations.', lower_bound=0)
flags.DEFINE_string('jdk', None, 'the jdk to use.')
flags.DEFINE_string('events', None, 'the perf events to measure.')
flags.DEFINE_boolean('static_analysis', None, 'perform static analysis.')
flags.DEFINE_string('bms', None, ''.join(('the benchmarks to run. Options:',
                                          'avrora, batik, eclipse, fop, h2, ',
                                          'jython, luindex, lusearch, pmd,',
                                          'sunflow, tomcat,tradebeans, ',
                                          'tradesoap, and xalan.')))
flags.DEFINE_boolean('all', None, 'runs all benchmarks.')
flags.DEFINE_string('ckjm', None, 'the ckjm jar file to use.')
flags.DEFINE_string('probes_path', None, 'the path to the probes.')
flags.DEFINE_string('callback', None, 'the DaCapo callback to use.')


def main(argv):
  del argv  # Unused.

  if FLAGS.debug:
    logging.set_verbosity(logging.DEBUG)
  else:
    logging.set_verbosity(logging.INFO)

  filtered_args = {flag: value for flag, value in
                   flags.FLAGS.flag_values_dict().iteritems() if value}
  run = Run()
  run.config(**filtered_args)

  if FLAGS.static_analysis:
    print run.run_ckjm()
  elif FLAGS.events:
    print run.run_perf()
  elif FLAGS.dacapo:
    print run.run_ckjm()
  else:
    print FLAGS.main_module_help()


if __name__ == '__main__':
  app.run(main)
