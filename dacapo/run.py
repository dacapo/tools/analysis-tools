"""Provides functions to run any analysis."""

from collections import deque
from parser import Parser
import tempfile
import zipfile
from absl import logging
from dacapo.command_builder import CommandBuilder
from report import Report
import util


class Run(object):
  """Class instance for running analysis.

  Instance Attributes:
    cmd_builder: a CommandBuilder object that constructs command to be executed.

    n: number of invocations.

    report: a Report object that stores the results for all the runs of this
    instance.
  """

  def __init__(self, n=1):
    """Constructs a new Run."""

    self._cmd_builder = CommandBuilder()
    self.n = n
    self.report = Report()

  def config(self, **kwargs):
    """Configure run with the desired parameters."""
    if 'w' in kwargs:
      self._cmd_builder.w = kwargs['w']
    if 'n' in kwargs:
      self.n = kwargs['n']
    if 'events' in kwargs:
      self._cmd_builder.events = kwargs['events']
    if 'jdk' in kwargs:
      self._cmd_builder.jdk = kwargs['jdk']
    if 'dacapo' in kwargs:
      self._cmd_builder.dacapo = kwargs['dacapo']
    if 'bms' in kwargs:
      self._cmd_builder.bms = kwargs['bms']
    if 'compiled' in kwargs:
      self._cmd_builder.compiled = kwargs['compiled']
    if 'interpreted' in kwargs:
      self._cmd_builder.interpreted = kwargs['interpreted']
    if 'static_analysis' in kwargs:
      self._cmd_builder.static_analysis = kwargs['static_analysis']
    if 'label' in kwargs:
      self.report.label = kwargs['label']
    if 'probes_path' in kwargs:
      self._cmd_builder.probes_path = kwargs['probes_path']
    if 'ckjm' in kwargs:
      self._cmd_builder.ckjm = kwargs['ckjm']
    if 'callback' in kwargs:
      self._cmd_builder.callback = kwargs['callback']

  def run_perf(self):
    """Starts a process to invoke the probes that measure perf events.

    Returns:
      report: an object containing all of the perf results.
    """
    parser = Parser()
    bms = self.get_benchmarks()

    logging.debug('Running benchmarks:%s', ','.join(bms))
    invocations = self.n
    logging.debug('Running for %d invocations', invocations)
    for i in range(invocations):
      for bm in bms:
        cmd = self._cmd_builder.construct_perf_cmd(bm)
        out = util.execute_cmd(cmd)
        results = parser.parse_probes(out)
        for _, dict_results in results.iteritems():
          self.report.add_dyn_analysis(bm, i, dict_results)

    return self.report

  def run_ckjm(self):
    """This will construct and execute the command for ckjm.

    Returns:
      report: an object containing all of the ckjm results.

    Raises:
      ValueError: if no benchmark or multiple benchmarks are specified.
    """

    bms = self.get_benchmarks()

    if len(bms) != 1:
      raise ValueError(('Static analysis should be performed on a single '
                        'benchmark'))

    bm = bms[0]

    if self._cmd_builder.static_analysis:
      parser = Parser()
      cmd = self._cmd_builder.construct_static_analysis_cmd(bm)
      out = util.execute_cmd(cmd)
      results, classes, _ = parser.parse_static_analysis(out)
      filter_results = {k: v for k, v in results.iteritems() if k in classes}
      self._run_loaded(bm, filter_results)
    else:
      self._run_from_jar(bm, self._cmd_builder.dacapo)

    return self.report

  def _run_loaded(self, bm, results):
    """Runs static analysis on loaded classes."""

    logging.debug('Running static analysis on loaded classes')

    list_of_classes = []
    for class_name, jar_name in results.iteritems():
      if class_name.endswith('.class'):
        class_name = class_name[:-6]
      class_name = class_name.replace('.', '/')
      class_name += '.class'

      if jar_name.startswith('file:'):
        jar_name = jar_name[5:]

      list_of_classes.append(' '.join([jar_name, class_name]))

    cmd = self._cmd_builder.construct_ckjm_cmd()
    out = util.execute_cmd(cmd, False,
                           input_value='\n'.join(list_of_classes))

    results = out.splitlines()

    self.report.add_static_analysis(bm, results)

  def _run_from_jar(self, bm, jar_file):
    """Runs static analysis from jar file."""

    logging.debug('Running static analysis from jar file.')
    classes = self._get_classes_from_jar(jar_file)
    list_of_classes = '\n'.join(['%s %s' % (jar, cl) for cl, jar
                                 in classes.iteritems()])
    cmd = self._cmd_builder.construct_ckjm_cmd()
    out = util.execute_cmd(cmd, False, input_value=list_of_classes)
    results = out.splitlines()
    self.report.add_static_analysis(bm, results)

  def _get_classes_from_jar(self, jar_file):
    """Gets all of the classes from a jar, including classes in nested jars.

    Args:
      jar_file: the jar file in which classes are extracted from:

    Returns:
      a dict that maps classes to their corresponding jar file.
    """

    queue = deque()
    dir_path = tempfile.mkdtemp()
    logging.debug('Creating temp directory: %s', dir_path)
    classes = {}
    queue.append(jar_file)

    while queue:
      jar_file = queue.popleft()
      logging.debug('Retrieving .class files from %s', jar_file)
      with zipfile.ZipFile(jar_file, 'r') as zp:
        file_list = zp.namelist()

        for filename in file_list:
          if filename.endswith('.class'):
            classes[filename] = jar_file
          elif filename.endswith('.jar') or filename.endswith('.zip'):
            logging.debug('Extracting %s from %s', filename, jar_file)
            tmp_jar = zp.extract(filename, dir_path)
            queue.append(tmp_jar)

    return classes

  def get_benchmarks(self):
    """Gets the list of benchmarks to run the analysis on."""
    if self._cmd_builder.bms:
      return self._cmd_builder.bms.split(',')
    else:
      return ['avrora', 'batik', 'eclipse', 'fop', 'h2', 'jython', 'luindex',
              'lusearch', 'pmd', 'sunflow', 'tomcat', 'tradebeans', 'tradesoap',
              'xalan'
             ]

  @property
  def command_builder(self):
    return self._cmd_builder
