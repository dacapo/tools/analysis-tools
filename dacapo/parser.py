"""Parses the output of the probes."""
import re
from absl import logging


def _make_parser_regex(list_of_tokens):
  """Constructs a regex for the parser given a list of tokens.

  Args:
    list_of_tokens: a list of tokens where the tokens are specified as
    (TYPE,<ID>), (TYPE,VALUE), or (TYPE). Note that <ID> associates an arbitrary
    value to the given ID. For instance, (NUMBER, <num>) will match (NUMBER, 1),
    where the id num is associated to the value 1.

  Returns:
    pattern: the corresponding regex from the list of tokens.
  """
  pattern = r''

  for token in list_of_tokens:
    if isinstance(token, tuple):
      if token[1].startswith('<') and token[1].endswith('>'):
        pattern += r'\(%s,(?P%s[^\(]+)\)' % token
      else:
        pattern += r'\(%s,%s\)' % token
    else:
      if token == '*ANY_NON_KEYWORD*':
        pattern += r'(\((ID|NUMBER|PUNCT)[^\(]*\))*'
      else:
        pattern += r'\(%s\)' % token
  return pattern


class Parser(object):
  """Parser for the output of the perf_launcher.py.

  Class Attributes:

    Token Representation
      _TOKEN: The representation of a token.
      _VALUED_TOKEN: The representation of a token with a value.

    Token types
      _NUMBER: an arbitrary rational number
      _STRING: an arbitrary collection of alphabetical characters, underscore
               and dash.
      _NEWLINE: a single newline character
      _WHITESPACE: any arbitrary number of whitespace character.
      _HEADER: four equal signs (i.e. ====) that represents a header from the
               Dacapo harness.
      _COMMENT: a sequence of 5 or more equal signs or a sequence of dash.
      _PUNCT: colon character
      _OTHER: any pattern than does not fall in any of the above.

    Lexer groups
      _keywords: words that have special meaning.
      _ignore: token types that are not important for the parser.

    Specifications
      _token_specification: specifies how a message is tokenized.
      _parser_specification: specifies how information is extracted from tokens.

    Regex definition
      _tok_regex: regex constructed from _token_specification.
      _parse_regex: regex constructed from _parser_specification.
  """

  ## Constants
  # Tokens
  _TOKEN = '(%s)'
  _VALUED_TOKEN = '(%s,%s)'
  # Token Types
  _NUMBER = r'[-]?\d+(\.\d*)?'
  _STRING = r'[A-Za-z][^\s\(\)\[\]:;,]*[A-Za-z0-9]'
  _NEWLINE = r'\n'
  _WHITESPACE = r'[\s]+'
  _HEADER = r'====='
  _COMMENT = r'([-]+|={6}[=]*).*([-]+|={6}[=]*)'
  _PUNCT = r'[:;,]'
  _LBRACKET = r'\['
  _RBRACKET = r'\]'
  _JAR = r'[^ ]+.jar'
  _OTHER = r'.'
 ## Keywords for the lexer analyzer
  _keywords = [
      'Setup', 'create_counter', 'Event_list', 'PERF_EVENT_NAMES', 'Event',
      'DaCapo', 'begin', 'end', 'report', 'CPU', 'starting', 'completed',
      'PASSED', 'FAILED',
  ]
  ## Token types that are skipped for parsing.
  _ignore = ['NEWLINE', 'WHITESPACE', 'COMMENT', 'OTHER']
  ## Specifications
  _token_specification = [
      ('JAR', _JAR),
      ('NUMBER', _NUMBER),
      ('STRING', _STRING),
      ('PUNCT', _PUNCT),
      ('NEWLINE', _NEWLINE),
      ('WHITESPACE', _WHITESPACE),
      ('LBRACKET', _LBRACKET),
      ('RBRACKET', _RBRACKET),
      ('COMMENT', _COMMENT),
      ('HEADER', _HEADER),
      ('OTHER', _OTHER),
  ]
  _parser_specification = [
      ('SETUP', _make_parser_regex([('KEYWORD', 'Setup'),
                                    ('*ANY_NON_KEYWORD*')
                                   ])),
      ('EVENT', _make_parser_regex([('KEYWORD', 'Event'),
                                    ('*ANY_NON_KEYWORD*')
                                   ])),
      ('COUNTER', _make_parser_regex([('KEYWORD', 'create_counter'),
                                      ('*ANY_NON_KEYWORD*')
                                     ])),
      ('LIST', _make_parser_regex([('KEYWORD', '(Event_list|PERF_EVENT_NAMES)'),
                                   ('*ANY_NON_KEYWORD*')
                                  ])),
      ('START_HEADER', _make_parser_regex([('HEADER'),
                                           ('KEYWORD', 'DaCapo'),
                                           ('*ANY_NON_KEYWORD*'),
                                           ('ID', '<bm>'),
                                           ('KEYWORD', 'starting'),
                                           ('*ANY_NON_KEYWORD*'),
                                           ('HEADER')
                                          ])),
      ('END_HEADER', _make_parser_regex([('HEADER'),
                                         ('KEYWORD', 'DaCapo'),
                                         ('*ANY_NON_KEYWORD*'),
                                         ('KEYWORD', 'completed'),
                                         ('*ANY_NON_KEYWORD*'),
                                         ('NUMBER', '<end_header>'),
                                         ('ID', 'msec'),
                                         ('HEADER')
                                        ])),
      ('SUCCESS_HEADER', _make_parser_regex([('HEADER'),
                                             ('KEYWORD', 'DaCapo'),
                                             ('*ANY_NON_KEYWORD*'),
                                             ('KEYWORD', 'PASSED'),
                                             ('ID', 'in'),
                                             ('NUMBER', '<succ_header>'),
                                             ('ID', 'msec'),
                                             ('HEADER')
                                            ])),
      ('FAIL_HEADER', _make_parser_regex([('HEADER'),
                                          ('KEYWORD', 'DaCapo'),
                                          ('*ANY_NON_KEYWORD*'),
                                          ('KEYWORD', 'FAILED'),
                                          ('HEADER')
                                         ])),
      ('BEGIN', _make_parser_regex([('KEYWORD', 'begin'),
                                    ('*ANY_NON_KEYWORD*'),
                                    ('NUMBER', '<begin>')
                                   ])),
      ('END', _make_parser_regex([('KEYWORD', 'end'),
                                  ('*ANY_NON_KEYWORD*'),
                                  ('NUMBER', '<end>')
                                 ])),
      ('REPORT', _make_parser_regex([('KEYWORD', 'report'),
                                     ('*ANY_NON_KEYWORD*'),
                                     ('NUMBER', '<report>')
                                    ])),
      ('RESULT', _make_parser_regex([('ID', '<name>'),
                                     ('PUNCT'),
                                     ('KEYWORD', 'CPU'),
                                     ('ID', 'all'),
                                     ('PUNCT'),
                                     ('NUMBER', '<value>')
                                    ])),
      ('OPENED', _make_parser_regex([('LBRACKET'),
                                     ('ID', 'Opened'),
                                     ('JAR', '<open_jar>'),
                                     ('RBRACKET')])),
      ('LOADED', _make_parser_regex([('LBRACKET'),
                                     ('ID', 'Loaded'),
                                     ('ID', '<class>'),
                                     ('ID', 'from'),
                                     ('JAR', '<loc_jar>'),
                                     ('RBRACKET')
                                    ])),
      ('COMPILED', _make_parser_regex([('ID', '<compiled_class>'),
                                       ('PUNCT'),
                                       ('PUNCT'),
                                       ('(ID|KEYWORD)', '<compiled_method>')
                                      ]))
      ]

  ## Regex from specification construction
  _tok_regex = '|'.join('(?P<%s>%s)' % pair
                        for pair in _token_specification)
  _parse_regex = '|'.join('(?P<%s>%s)' % pair
                          for pair in _parser_specification)

  def __init__(self):
    pass

  def parse_probes(self, msg):
    """Parses out the output of the probes and retrieves analysis results.

    Args:
      msg: stdout from the probes.

    Returns:
      a dictionary with dynamic analysis results
    """
    logging.debug('Parsing results from probes')
    tokens = self._lexer(msg)
    return self._parser_for_probes(''.join(tokens))

  def parse_static_analysis(self, msg):
    """Parses out the output of the probes and retrieves analysis results.

    Args:
      msg: stdout from Java running static analysis.

    Returns:
      a dictionary with loaded classes mapped to a jar file, a list of loaded
      classes, and a list of executed methods.
    """
    logging.debug('Parsing results for static analysis')
    tokens = self._lexer(msg)
    return self._parser_static_analysis(''.join(tokens))

  def _lexer(self, msg):
    """Tokenizes a given message.

    Args:
      msg: a message string corresponding to the output of the perf_launcher.py.

    Returns:
      a list of tokens.
    """

    tokens = []

    for mo in re.finditer(self._tok_regex, msg):
      kind = mo.lastgroup
      value = mo.group(kind)
      if kind in self._ignore:
        continue
      elif kind == 'STRING':
        if value in self._keywords:
          tokens.append(self._VALUED_TOKEN % ('KEYWORD', value))
        else:
          tokens.append(self._VALUED_TOKEN % ('ID', value))
      elif kind == 'NUMBER' or kind == 'JAR':
        tokens.append(self._VALUED_TOKEN % (kind, value))
      else:
        tokens.append(self._TOKEN % kind)

    return tokens

  def _parser_for_probes(self, tokens_str):
    """Parses the tokens generated.

    Args:
      tokens_str: tokens generated from the lexer.

    Returns:
      result: a dictionary with the parsed results.

    Raises:
      ValueError: when a report iteration and the execution iteration do not
      match.
    """

    iteration = -1
    bm = None

    result = {}

    for mo in re.finditer(self._parse_regex, tokens_str):
      kind = mo.lastgroup

      if kind == 'BEGIN':
        # Iteration starts at 1 so needs to offset it
        iteration = int(mo.group('begin')) - 1
        # Resets benchmark because it might be a different one
        bm = None
      elif kind == 'REPORT':
        if iteration != int(mo.group('report')) - 1:
          raise ValueError('A report is generated in the wrong iteration.')
      elif kind == 'START_HEADER':
        bm = mo.group('bm')

        if bm not in result:
          result[bm] = {}

      elif kind == 'END_HEADER':
        self._set_metric_value(result,
                               iteration,
                               bm,
                               'time',
                               mo.group('end_header'))
      elif kind == 'SUCCESS_HEADER':
        self._set_metric_value(result,
                               iteration,
                               bm,
                               'time',
                               mo.group('succ_header'))
      elif kind == 'RESULT':
        self._set_metric_value(result,
                               iteration,
                               bm,
                               mo.group('name'),
                               mo.group('value'))

    return result

  def _parser_static_analysis(self, tokens_str):
    """Parses the tokens generated.

    Args:
      tokens_str: tokens generated from the lexer.

    Returns:
      result: a dictionary with the loaded classes mapped to their corresponding
      jar.
      classes: a set of classes that had a method compiled.
      methods: a set of methods compiled.
    """

    opened_jars = set()
    classes = set()
    methods = set()
    static_analysis = {}

    for mo in re.finditer(self._parse_regex, tokens_str):
      kind = mo.lastgroup
      if kind == 'OPENED':
        opened_jars.add(mo.group('open_jar'))
      elif kind == 'LOADED':
        jar_file = mo.group('loc_jar')
        class_file = mo.group('class')
        if jar_file not in opened_jars:
          static_analysis[class_file] = jar_file
      elif kind == 'COMPILED':
        class_name = mo.group('compiled_class')
        method_name = mo.group('compiled_method')
        classes.add(class_name)
        methods.add('::'.join([class_name, method_name]))
    return static_analysis, classes, methods

  def _set_metric_value(self, result, iteration, bm, metric, value):
    """Stores an analysis result."""

    if iteration >= 0 and bm:
      bm_result = result[bm]

      if metric not in bm_result:
        bm_result[metric] = []

      if len(bm_result[metric]) != iteration:
        raise ValueError(('A report has been raised in the wrong iteration ',
                          ' for benchmark %s and metric %s ' % (bm, metric),
                          'at iteration %d.' % iteration))
      bm_result[metric].append(value)
    else:
      raise ValueError(('A metric is obtained but the iteration or '
                        'benchmark is unknown.'))
