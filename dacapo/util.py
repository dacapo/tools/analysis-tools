"""Utility functions for the analysis tools.
"""
import subprocess
from absl import logging


def execute_cmd(cmd, include_stderr=True, input_value=None):
  """Executes a given command.

  Args:
    cmd: an array of strings containing the command to execute.
    include_stderr: if the stderr should be captured along with stdout.
    input_value: if there is a value that should be passed as stdin to the
    process.

  Raises:
    RuntimeError: when something goes wrong when running the probes.

  Returns:
    out: the stdout and stderr from the executed command.
  """

  stdout = subprocess.PIPE
  stderr = subprocess.PIPE
  stdin = None

  if include_stderr:
    stderr = subprocess.STDOUT
  if input_value:
    stdin = subprocess.PIPE

  logging.debug('Running command: %s', ' '.join(cmd))
  process = subprocess.Popen(cmd,
                             stdin=stdin,
                             stderr=stderr,
                             stdout=stdout)

  out, _ = process.communicate(input_value)

  if process.returncode == 0:
    return out

  msg = '''An error occurred!
Executed command:
%s.

Return code:
%s

Output message:
%s''' % (' '.join(cmd), process.returncode, out)

  raise RuntimeError(msg)
