"""Plotter for generating line plots of dynamic analysis data."""
from absl import logging
import matplotlib.pyplot as plt
import pandas as pd


class Plotter(object):
  """Class for plotting analyses results.

      Instance Attributes:
        reports: a list of Report objects.

        bm: the benchmarks that will be plotted.

        metric: the metrics that will be plotted.

        separate: if normalization happens separately or to same factor among
        all reports.

        best: if normalizing to the lowest value

        normalize: index of the iteration or the metric that is used to
        normalize each report.

        per: divide metrics by the per variable to combine metrics (e.g. branch
        misses per total branch instructions).

        log: if the y-axis should use log scaling.
  """

  def __init__(self):
    """Creates an empty instance of Plotter."""
    self._reports = []
    self._bm = None
    self._metric = None
    self._separate = False
    self._best = True
    self._normalize = None
    self._per = None
    self._ref_metric = None
    self._use_metric_to_normalize = True
    self._kind = 'best'
    self._log = False

  def config(self, **kwargs):
    """Configure plotter with the desired parameters."""

    if 'reports' in kwargs:
      self.reports = kwargs['reports']
    if 'bm' in kwargs:
      self.bm = kwargs['bm']
    if 'separate' in kwargs:
      self.separate = kwargs['separate']
    if 'best' in kwargs:
      self.best = kwargs['best']
    if 'normalize' in kwargs:
      self.normalize = kwargs['normalize']
    if 'per' in kwargs:
      self.per = kwargs['per']
    if 'metric' in kwargs:
      self.metric = kwargs['metric']
    if 'log' in kwargs:
      self.log = kwargs['log']

  def plot(self, filename=None):
    """Plots the result of reports normalized to the best/worst among them.

    Args:
      filename: the name of the file in case the plots need to be exported.
    Returns:
      the constructed figure and axis as well as the data that was plotted.

    Raises:
     TypeError: if reports is not iterable.
    """
    fig, ax = plt.subplots()

    ymean = None
    yerr = None

    if self._reports and self._bm:
      logging.debug('Plotting: %s', self._bm)
      if self._separate:
        logging.debug('Normalization is computed independently')
        ymean, yerr = self._plot_independent_normalized(ax)
      else:
        logging.debug('Normalization is computed among all reports')
        ymean, yerr = self._plot_normalized(ax)
      self._output_plot(filename)
    else:
      logging.debug('Not enough information is provided for plotting')
    return fig, ax, ymean, yerr

  def _output_plot(self, filename):
    """Save to file if a name is provided. Otherwise, display to the user."""
    if filename:
      logging.debug('Saving plot to %s', filename)
      plt.savefig(filename, bbox_inches='tight')
    else:
      logging.debug('Displaying the plot')
      plt.show()

  def _plot_normalized(self, ax):
    """Plots the results normalized to the same values among all reports."""
    abs_ax = ax.twinx()

    ymean, yerr, lim = self.get_mean_dataframe()

    ymean.plot(kind='line', ax=abs_ax, marker='o', yerr=yerr, logy=self._log,
               legend=True, elinewidth=1.5, capsize=3, capthick=1.5, lw=2, ms=5)

    plt.title(self._bm)
    plt.xticks(range(len(ymean.index)))

    ylim = abs_ax.get_ylim()

    # Adjust normalized axis. Best normalizes to smallest and worst to the
    # biggest value.
    if self._log:
      ax.set_yscale('log')

    if self._best:
      ax.set_ylim(bottom=ylim[0]/lim[0], top=ylim[1]/lim[0], auto=True)
    else:
      ax.set_ylim(bottom=ylim[0]/lim[1], top=ylim[1]/lim[1], auto=True)

    ax.set_ylabel(self._get_normalize_label())
    ax.yaxis.grid(True, alpha=0.7, which='both')
    abs_ax.set_ylabel('%s\nraw value' % self._ref_metric)

    self._display_footnote(ax)

    return ymean, yerr

  def _plot_independent_normalized(self, ax):
    """Plots the results of reports normalized independenly."""

    ymean, yerr, max_iterations = self.get_norm_dataframes()
    ax.set_xlabel('Iteration')
    ax.set_ylabel(self._get_normalize_label())

    ymean.plot(kind='line', ax=ax, yerr=yerr, logy=self._log, marker='o', ms=5,
               legend=True, elinewidth=1.5, capsize=3, capthick=1.5, lw=2)

    plt.title(self._bm)
    plt.xticks(range(max_iterations))
    ax.yaxis.grid(True, alpha=0.7, which='both')
    self._display_footnote(ax)

    return ymean, yerr

  def _display_footnote(self, ax):
    """Displays footnote on the axis to indicate what N and error bars mean."""
    footnote = '\n'.join(['* The error bars report 95% confidence interval.',
                          '* N represents the number of invocations.'])
    ax.text(0, -0.25, footnote, transform=ax.transAxes, size='small')

  def get_mean_dataframe(self):
    """Constructs a dataframe with the mean of a collection of reports.

    Returns:
      dataframe with the mean values, dataframe with the standard deviation
      values and the minimum and maximum values relative to the best/worst
      iteration values relative to the normalization factor.

    Raises:
      ValueError: If the normalize value is not a valid row or column.
    """

    df_mean = pd.DataFrame()
    df_err = pd.DataFrame()
    report_count = 0

    min_val = None
    max_val = None

    for report in self.reports:
      df = pd.DataFrame()
      norm_ref_df = pd.DataFrame()

      dyn_analysis = report.get_dyn_analysis(self._bm)
      for i in range(dyn_analysis.size):
        abs_df = dyn_analysis.get_invocation(i)
        if self._per:
          abs_df = self.scale_column(abs_df)
        df[str(i)] = abs_df[self._ref_metric]
        norm_ref_df = norm_ref_df.add(abs_df, fill_value=0)

      mean = df.mean(axis=1)
      err = df.std(axis=1)
      df_mean[str(report_count)] = mean
      df_err[str(report_count)] = 2*err
      report_count += 1

      norm_ref = self.normalize_dataframe(norm_ref_df)

      if isinstance(self._normalize, str):
        curr_min = mean[norm_ref[self._normalize].idxmin()]
        curr_max = mean[norm_ref[self._normalize].idxmax()]
      elif isinstance(self._normalize, int):
        curr_min = mean[self._normalize]
        curr_max = mean[self._normalize]
      else:
        raise TypeError(('Normalization to %s is invalid ' % self._normalize,
                         'because is not a valid attribute.'
                        ))
      if min_val is None:
        min_val = curr_min
      elif curr_min < min_val:
        min_val = curr_min

      if max_val is None:
        max_val = curr_max
      elif curr_max > max_val:
        max_val = curr_max

    labels = self._get_report_labels()

    df_mean.columns = labels
    df_err.columns = labels

    return df_mean, df_err, [min_val, max_val]

  def get_norm_dataframes(self):
    """Merge all normalized invocations of a metric into a single dataframe.

    Returns:
      a collection of normalized dataframes indepedently.
    """
    df_mean = pd.DataFrame()
    df_err = pd.DataFrame()
    report_count = 0
    max_iterations = 0

    for report in self._reports:
      df = pd.DataFrame()
      dyn_analysis = report.get_dyn_analysis(self._bm)

      for i in range(dyn_analysis.size):
        abs_df = dyn_analysis.get_invocation(i)
        if self._per:
          abs_df = self.scale_column(abs_df)
        norm_df = self.normalize_dataframe(abs_df)
        df[str(i)] = norm_df[self._ref_metric]

      mean = df.mean(axis=1)
      err = df.std(axis=1)

      df_mean[str(report_count)] = mean
      df_err[str(report_count)] = 2*err

      report_count += 1

      if len(mean.index) > max_iterations:
        max_iterations = len(mean.index)

    labels = self._get_report_labels()

    df_mean.columns = labels
    df_err.columns = labels

    return df_mean, df_err, max_iterations

  def scale_column(self, df):
    """Normalize data by the smallest number of a given metric.

    Args:
      df: a dataframe.

    Returns:
      a new dataframe with a new column containing the reference column scaled
      by another.

    Raises:
      ValueError: if either column is not present in the dataframe.
    """

    if self._metric not in df:
      raise ValueError(('Cannot scale data %s ' % self._ref_metric,
                        'because %s is not valid.' % self._metric))
    if self._per not in df:
      raise ValueError(('Cannot scale data %s ' % self._ref_metric,
                        'because %s is not valid.' % self._per))

    df_scaled = (df[self._metric] / df[self._per]).to_frame(self._ref_metric)
    df = pd.concat([df, df_scaled], axis=1)

    return df

  def _get_metric_label(self):
    """Gets the metric label adjusted to a scaling metric."""
    if self._per:
      return '%s per %s' % (self._metric, self._per)
    else:
      return self._metric

  def _get_normalize_label(self):
    """Gets metric adjusted according to normalization."""

    if isinstance(self._normalize, int):
      label = '\n'.join([self._ref_metric, 'normalized to %s iteration %s'
                         % (self._kind, self._normalize)])
    else:
      label = '\n'.join([self._ref_metric, 'normalized to %s %s'
                         % (self._kind, self._normalize)])
    return label

  def _get_report_labels(self):
    """Gets a list of labels associated with a collection of reports."""
    labels = []
    for report in self._reports:
      label = '%s N=%d' % (report.label, report.get_dyn_analysis(self._bm).size)
      labels.append(label)
    return labels

  def normalize_dataframe(self, df):
    """Normalizes dataframe relative to a row or to a column.

    When normalizing to a column, the row with the best value in that column is
    used to normalize the dataframe.

    Args:
      df: a dataframe that will be normalized.

    Returns:
      a normalized dataframe.

    Raises:
      TypeError: if the normalization attribute has the wrong type.
    """
    if self._use_metric_to_normalize:
      self._normalize = self._ref_metric

    if isinstance(self._normalize, str):
      norm_df = self._normalize_by_metric(df)
    elif isinstance(self._normalize, int):
      norm_df = self._normalize_by_index(df)
    else:
      raise TypeError(('Normalization to %s is  ' % self._normalize,
                       'invalid because is not a valid attribute. You can ',
                       'normalize the values of a given iteration or to ',
                       'a measured metric.'
                      ))
    return norm_df

  def _normalize_by_metric(self, df):
    """Normalize data by the smallest number of a given metric."""

    if self._normalize not in df:
      raise ValueError(('Cannot normalize data to %s ' % self._normalize,
                        'because this column does not exist.'))

    if self._best:
      val_index = df[self._normalize].idxmin()
    else:
      val_index = df[self._normalize].idxmax()
    # Divide every column by the value on the row of the smallest element of
    # the specified metric.
    return df.apply(lambda x: x.div(x[val_index]), axis=0)

  def _normalize_by_index(self, df):
    """Normalize data by the values of a given row."""

    if self._normalize < 0 or self._normalize > len(df):
      raise IndexError(('Cannot normalize data to %s ' % self._normalize,
                        'because the row does not exist.'))

    # Divide every column by the value from the row of the given index.
    return df.apply(lambda x: x.div(x[self._normalize]), axis=0)

  @property
  def reports(self):
    return self._reports

  def add_report(self, report):
    self._reports.append(report)

  @reports.setter
  def reports(self, reports):
    if reports is None:
      self._reports = []
    else:
      try:
        iter(reports)
      except TypeError as err:
        raise err
      self._reports = reports

  @property
  def bm(self):
    return self._reports

  @bm.setter
  def bm(self, bm):
    self._bm = bm

  @property
  def metric(self):
    return self._metric

  @metric.setter
  def metric(self, metric):
    self._metric = metric
    self._ref_metric = self._get_metric_label()

  @property
  def separate(self):
    return self._separate

  @separate.setter
  def separate(self, separate):
    self._separate = separate

  @property
  def best(self):
    return self._best

  @best.setter
  def best(self, best):
    self._best = best

    if best:
      self._kind = 'best'
    else:
      self._kind = 'worst'

  @property
  def normalize(self):
    return self._normalize

  @normalize.setter
  def normalize(self, normalize):
    if normalize is None:
      self._use_metric_to_normalize = True
    else:
      self._use_metric_to_normalize = False
      self._normalize = normalize

  @property
  def per(self):
    return self._per

  @per.setter
  def per(self, per):
    self._per = per
    self._ref_metric = self._get_metric_label()

  @property
  def log(self):
    return self._log

  @log.setter
  def log(self, log):
    self._log = log
