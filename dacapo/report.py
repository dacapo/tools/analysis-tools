"""Records analysis data."""
from absl import logging
from dacapo.dynamic_analysis import DynamicAnalysis
from dacapo.static_analysis import StaticAnalysis
import pandas as pd


class Report(object):
  """Stores a collection of data from static and dynamic analysis."""

  def __init__(self, label='default'):
    """Constructs a new Report."""
    self._static_analysis = {}
    self._dyn_analysis = {}
    self._label = label

  def add_dyn_analysis(self, bm, invocation, dict_results):
    """Adds a dynamic analysis result to the report.

    Args:
      bm: the benchmark that the results correspond to.
      invocation: the invocation index of the given results.
      dict_results: associates metric to results.
    """
    logging.debug('Adding dynamic analysis results')
    df = pd.DataFrame(dict_results)
    if bm not in self._dyn_analysis:
      self._dyn_analysis[bm] = DynamicAnalysis()

    self._dyn_analysis[bm].add_invocation(invocation, df)

  def get_dyn_analysis(self, bm):
    """Gets a particular DynamicAnalysis instance.

    Args:
      bm: the selected benchmark's results that is being retrieved.

    Raises:
      ValueError: if specified benchmark has not been setup.

    Returns:
      a DynamicAnalysis object associated with the given benchmark.
    """

    if bm not in self._dyn_analysis:
      raise ValueError('Dynamic analysis for benchmark %s not exist.' % bm)

    return self._dyn_analysis[bm]

  def add_static_analysis(self, bm, list_results):
    """Adds a static analysis result to the report.

    This function will run static analysis on the given inputs and store the
    results to a StaticAnalysis object.

    Args:
      bm: the benchmark that the results correspond to.
      list_results: a list of string corresponding to each line spit out from
      ckjm.

    Raises:
      TypeError: if the input is not a dict or string.
    """
    logging.debug('Adding static analysis results')
    if bm not in self._static_analysis:
      self._static_analysis[bm] = StaticAnalysis()

    static_analysis = self._static_analysis[bm]

    for result in list_results:
      static_analysis.store_result(result)

  def get_static_analysis(self, bm):
    """Gets a particular StaticAnalysis instance.

    Args:
      bm: the selected benchmark's results that is being retrieved.

    Raises:
      ValueError: if specified benchmark has not been setup.

    Returns:
      a StaticAnalysis object associated with the given benchmark.
    """

    if bm not in self._static_analysis:
      raise ValueError('Static analysis for benchmark %s not exist.' % bm)
    return self._static_analysis[bm]

  def save_csv(self):
    """Save static and dynamic analyses to csv."""
    for bm, dyn_analysis in self._dyn_analysis.iteritems():
      prefix = '%s_dynamic_analysis_%s' % (self._label, bm)
      dyn_analysis.save_csv(prefix)

    for bm, static_analysis in self._static_analysis.iteritems():
      prefix = '%s_static_analysis_%s' % (self._label, bm)
      static_analysis.save_csv(prefix)

  @property
  def static_analysis(self):
    """Returns the static analysis object associated with this report."""
    return self._static_analysis

  @property
  def number_of_dyn_analyses(self):
    """Returns the number of benchmarks with dynamic analysis results."""
    return len(self._dyn_analysis)

  @property
  def number_of_static_analyses(self):
    """Returns the number of classes with static analysis results."""
    return len(self._static_analysis)

  @property
  def bms(self):
    """Returns the list of benchmarks this report has dynamic data for."""
    return self._dyn_analysis.keys()

  @property
  def label(self):
    """Returns the label that describes this report."""
    return self._label

  @label.setter
  def label(self, value):
    """Sets the value of the label that describes this report."""
    self._label = value

  def __repr__(self):
    string_values = []
    if self._dyn_analysis:
      string_values.append('### Dynamic Analysis ###')
      for bm, val in self._dyn_analysis.iteritems():
        string_values.append('Benchmark: %s' % bm)
        string_values.append(repr(val))

    if self._static_analysis:
      string_values.append('### Static Analysis ###')
      for bm, val in self._static_analysis.iteritems():
        string_values.append('Benchmark: %s' % bm)
        string_values.append(repr(val))
    return '\n'.join(string_values)

