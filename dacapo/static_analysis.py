"""Stores the results of static analysis."""
import pandas as pd


class StaticAnalysis(object):
  """Results for static analysis.

    The following metrics are computed:
      wmc:  Weighted methods per class
      dit:  Depth of Inheritance Tree
      noc:  Number of Children
      cbo:  Coupling between object classes
      rfc:  Response for a Class
      lcom: Lack of cohesion in methods
      ca:   Afferent couplings
      npm:  Number of Public Methods

  Instance Attributes:
    command_builder: CommandBuilder object that constructs the command to invoke
    ckjm.
  """

  def __init__(self):
    self._df = pd.DataFrame(columns=['wmc', 'dit', 'noc', 'cbo',
                                     'rfc', 'lcom', 'ca', 'npm'])

  @property
  def size(self):
    """The number classes with static analysis result."""
    return len(self._df.index)

  def store_result(self, value):
    """Stores the result from the ckjm tool.

    Args:
      value: this is formatted as 'class wmc dit noc cbo rfc lcom ca npm'
      so this function will split the metrics and store them as a new row in the
      dataframe.

    Raises:
      ValueError: if the input to this function is not formatted properly.
    """

    parsed_result = value.rstrip().split()

    if len(parsed_result) != 9:
      raise ValueError(('Tried to store the value "%s" but it ' % value,
                        'is not properly formatted. The value should be as ',
                        'follows: "class wmc dit noc cbo rfc lcom ca npm", ',
                        'where class is a string and the other values are ',
                        'integers.'))
    class_name = parsed_result[0]
    values = [float(x) for x in parsed_result[1:9]]
    self._df.loc[class_name] = values

  def get_result_of_class(self, class_name):
    """Gets static analysis result for a particular class name.

    Args:
      class_name: the name of the class that you want to get data from.

    Returns:
      static analysis results for the given class.
    """
    return self._df.loc[class_name]

  def get_mean(self):
    """Get the mean values for each static analysis metric."""
    return self._df.mean()

  def get_min(self):
    """Get the min values for each static analysis metric."""
    return self._df.min()

  def get_max(self):
    """Get the max values for each static analysis metric."""
    return self._df.max()

  def save_csv(self, prefix):
    """Saves static analysis results to a csv file."""
    self._df.to_csv('%s.csv' % prefix)

    # Save mean, min, and max
    self.get_mean().to_csv('%s_mean.csv' % prefix)
    self.get_min().to_csv('%s_min.csv' % prefix)
    self.get_max().to_csv('%s_max.csv' % prefix)

  def __repr__(self):
    return repr(self._df)
