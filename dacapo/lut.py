"""Look up table for elephant tracks traces."""

import os
import re


class LookUpTable(object):
  """Stores the classes, fields, and methods and their associated id.

  Instance Attributes:
    methods: dict that associates id to method name.
    classes: dict that associates id to class name.
    fields: dict that associates id to field name.
  """

  def __init__(self):
    """Constructs a new LUT object."""
    self.methods = {}
    self.classes = {}
    self.fields = {}

  def setup(self, directory=None):
    """Reads and stores the list files produced from Elephant Tracks.

    Args:
      directory: where directory where the list files are located. If none is
      specified, then the list files are assumed to be in the current directory.
    """

    methods_list = 'methods.list'
    fields_list = 'fields.list'
    classes_list = 'classes.list'

    if directory:
      methods_list = os.path.join(directory, methods_list)
      fields_list = os.path.join(directory, fields_list)
      classes_list = os.path.join(directory, classes_list)

    self._add_entries_from_file(methods_list, self.methods)
    self._add_entries_from_file(fields_list, self.fields)
    self._add_entries_from_file(classes_list, self.classes)

  def _add_entries_from_file(self, filename, collection):
    """Reads list from Elephant Tracks and stores the entries from the list."""
    with open(filename, 'r') as et_file:
      for line in et_file:
        key, value = self._extract_value_key(line)
        collection[key] = value

  def _extract_value_key(self, line):
    """Extracts the id and the associated value from an entry."""
    line = self._format_string(line)
    values = line.split(',')

    # If there is a third part, it is the method name, merge it now.
    if len(values) >= 3:
      values[1] = '.'.join(values[1:])

    return values[0], values[1]

  def _format_string(self, string_value):
    """Formats fields, methods, and classes to use '.' as separator."""
    updated_string = re.sub(r'\s', '', string_value)
    return re.sub(r'[/]', '.', updated_string)
